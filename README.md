# About
The project is for data analysis related to Tamil Nadu Legislative Assembly Elections 2021.

# What it does?

* Originally written to spot [Symbol Phishing Attacks](https://docs.google.com/spreadsheets/d/12wxe-zr5c9RZKgMOAWxqqf-3LBfTQKnNFOnENyBfXtU/edit#gid=0) - to highlight [Pen Nib With Seven Rays Phishing Attack on Rising Sun](https://docs.google.com/spreadsheets/d/e/2PACX-1vT7v-ay5yEXNKcj7mCATiCaQiW4pnpHsK18fMAwcJIkRqx2c6Im8dsMzZLGxAWykq6-gLLbCAO9ur27/pubhtml?gid=0&single=true) and [Electric Pole Phishing Attack on Two Leaves](https://docs.google.com/spreadsheets/d/e/2PACX-1vT7v-ay5yEXNKcj7mCATiCaQiW4pnpHsK18fMAwcJIkRqx2c6Im8dsMzZLGxAWykq6-gLLbCAO9ur27/pubhtml?gid=511692580&single=true). The script scrapped [Form7A](https://www.elections.tn.gov.in/TNLA2021_Form7A.aspx) from election commission's website and filter candidates contesting with Pen Nib with Seven Rays and Electric Pole symbols.
* We now aim to make a database of free symbols, machine readable candidate list, visualize results.