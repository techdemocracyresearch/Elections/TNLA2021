#!/bin/bash
mkdir -p TNLA2021/Form7A/English/Raw
mkdir -p TNLA2021/Form7A/English/OCR
mkdir -p TNLA2021/Form7A/English/Text
cd TNLA2021/Form7A/English/Raw
wget https://www.elections.tn.gov.in/TNLA2021_Form7A/English/AC{001..231}.pdf
cd TNLA2021/Form7A/English/Raw
for f in *.pdf; do ocrmypdf $f ocr_$f; done; mv ocr_*.pdf ../OCR
mv ocr_*.pdf ../OCR
#---- Manually move Tilted files to seperate folder
cd Tilt
for f in *.pdf;do ocrmypdf --rotate-pages --rotate-pages-threshold 2.0 $f ocr_$f; done;
mv ocr_*.pdf ../../OCR
cd ../../OCR
for f in *.pdf; do pdftotext $f txt_$f.txt; done;	mv txt_*.txt ../Text;
cd ../Text
grep -E -i 'NIB' *.txt
#---- Take this list to Google Sheet with VLOOKUP from Wikipedia Page
grep -E -i 'POLE' *.txt


mkdir -p TNLA2021/Form7A/Tamil/Raw
mkdir -p TNLA2021/Form7A/Tamil/OCR
mkdir -p TNLA2021/Form7A/Tamil/Text
cd TNLA2021/Form7A/Tamil/Raw
wget https://www.elections.tn.gov.in/TNLA2021_Form7A/Tamil/AC{001..231}.pdf
ocrmypdf --rotate-pages --rotate-pages-threshold 2.0 AC118.pdf AC118_ocr_1.pdf
pdftotext AC001_ocr.pdf AC001_txt.txt
