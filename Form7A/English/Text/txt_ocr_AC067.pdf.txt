FORM 7A
LIST OF CONTESTING CANDIDATES.
[SEE RULE 10(1) OF THE CONDUCT OF ELECTIONS RULES, 1961]
Election to the Tamilnadu Legislative Assembly
From the 67.Arani Assembly Constituency.

SI.
No.

(1)

1

:
Name of candidate

Candidate's
photo

,
Address of candidate

oes
Party affiliation

Symbol
allotted

(2)

(3)

(4)

(5)

(6)

'
Dravida Munnetra
Kazhagam

anc
Rising Sun

(i) Candidates of recognised National and State Political Parties.
No.402/2
Mettu Street
a
Agrapalayam Village,

Anbalagan .S.S

Arni Taluk,
Tiruvannamalai District.

f

te

Anbu .

nburts

pn

4

Aniyalai Village
Padagam Post,

=)

Kalasapakkam Taluk,

/

E

3 | Baskaran .G

!

)

Elephant

Thiruvannamalai District.

f- ‘

ies

Bahujan Samaj

Party

No. 32,

-

Thennamara Street,

Desiya Murpokku

Arni Town & Taluk

Kazhagam

Kosapalayam,

Tiruvannamalai District

4 | Sevoor Ramachandran .S
1

Dravida

aren
ste
Sevoor Village & Post,

ADravida
nae
Munnetra

Arni Taluk,

Kazhagam

Tiruvannamalai District.

Nagara

Twoleaves

ii) Candidates of Registered Political Parties
(Other than recognised National and State Political Parties)

5 | Thanigaivel .S

No. 400,
Ponniyamman Koil Street,
Thatchur Village & Post,

Arni Taluk,
Tiruvannamalai

District.

Veerath Thiyagi
Viswanatha doss
Thozhilalarkal
Katchi

Pest
d
M7 Bees

No. 512, 1* Street,
Pudhupalaiyam

Onnupuram Post,
Arni Taluk,
Tiruvannamalai District.

Prakalatha .D

No. 125, Palla Street,

Manikandan .V

Naam Tamilar
Katchi

Aarasur Village & Post,

Makkal Needhi
Maiam

No. 141, P.R.G Housing
Arasu City,
lrumbedu Village,
Arni Taluk,
Tiruvannamalai District.

Independent

Vandavasi Taluk,
Tiruvannamalai District.

Ganna Kisan

Battery Torch

iii) Other candidates

oo

Arun Kumar .A

Anbalagan .K

Indira Nagar Colony,
Sangeethavadi Village,

Arni Taluk,

Dust Bin

Independent

Dish Antenna

Independent

Stool

Independent

Peas

Tiruvannamalai District.

No.60/2,

10

11

Anbalagan .M

Karthikeyan .N

Road Street,

Velleri Village,

Arni Taluk,
Tiruvannamalai District.

No.109,
Kalathu Mettu Street,
Kosapalaiyam,
Arni Town & Taluk,
Tiruvannamalai District.

12

No. 870,
Aringer Anna Street,
Paiyur Village,
Arni Taluk,
Tiruvannamalai District.

Sakthivel .S

Independent

Petrol Pump

Independent

Sofa

Independent

Matchbox

Independent

Nail Cutter

No, 36, Palla Street,

Maduraiperumpattur

13

14

Dhatchanamurthy .R

Village,
Thellur Post,
Chetpet Taluk,
Tiruvannamalai District.
No.412,
Bajanai Kovil Street,
Velumanthangal Village,
Melmattai Vinnamangalam
Post,

Murali .S

Cheyyar Taluk,

Tiruvannamalai District.

15

Ramachandran

Place:

Date

.S

Arani

: 22.03.2021

No,41 Ambedkar Nagar,
Arni Town & Taluk,
Tiruvannamalai District.

Chkgce

(R. POONKODI)
ey

ipo

LiiCeL,

itucney”

Uiieer, Aan

