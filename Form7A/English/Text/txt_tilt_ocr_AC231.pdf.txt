FORM 7-A
[See rule 10{1}]
LIST OF CONTESTING

CANDIDATES

General Election to the Tamil Nadu Legislative Assembly 2021
From the 231, Colachel Assembly Constituency

SI. No.

Name of Candidate

Candidate’s Photo

mOdness os

(1)

(2)

(3)

(4)

candidate

Party affiliation | Symbol Allotted

(5)

(6)

(i) Candidates of recognised National and State Political parties

I.

AnH

6

f .

dhe

aon

Gs

4

26/28, Eastern street,
.

Eraniel,

oe,

Neyyoor post-

be a

iodo.

’

2.

Sivakumar M.

3.

PHnee Ge

en

38

ce”

)

2-193

a

as

rajar

seiner:
.
Ganapathipuram
Post.

Street, Thekkurichi,

Samavilai,

7

Party

629802

Se cee
Woy
o%
ae
2
ga . bes

¢

Bahujan Samaj

Perumpuli,
Unnamalaikadai,
Kanjiracode Post.

Elephant

Desiya Murpokku
.
Dravida
Kazhagam
a 2

Nagara

Indian National

Hand

Congress

Ramesh

P.

2/24, Kumari Balan
Illam,
Brammapuram,
Kumarakoil Post.

Bharatiya Janata
Party

Lotus

(ii) Candidates of registered Political parties.

(Other than recognised National and State Political parties)

Anthony Muthu S.M.

Antony Aslin J.

Communist Party

13-77B(66),
Patharai,

Reethapuram Post —
629159
22/139, Punnakala
Vilai, Kadayal,

Kaliyal Post —

629101

Krishna Kumar A.

3/92, Kulalar street,
Thalakulam

Post -

629802

Raja C.

29/57,
Paruththikattuvilai,
Thiruvithancode

of India (Marxist-

Leninist)
(Liberation)

Naam Tamilar
Katchi

Flag with Three
Stars

Ganna Kisan

Anaithu Makkal
puratchi Katchi

Glass Tumbler

Shiv Sena

Air Conditioner

629174

Lathis Mary S.

18, 2" Street,
Manikandapuram,
Thirumullaivayal,
Chennai - 600062

Makkal Needhi
Maiam

Battery Torch

(iii) Other candidates.

Mohammed Ali,

Professor, S.

Marystella R.

Vijayakumar R.

wi

seal

ae
es

Nial

8/20, Manavilai,
Muslim street,

Vellichanthai Post —

Independent

629203.

9/IATA,
Ezhuvilaipattu,
Alanji Post- 629 159

9/59C, Alpattuvilai,
Kurunthencode Post

- 629805

Independent

Auto-Rickshaw

Independent

Place : Kurunthencode

Date : 22.03.2021.
Returning Officer,

231.Colachel Assembly Constituency

and District Adi Dravidar and Tribal Welfare officer
Nagercoil.

