FORM 7-A.
LIST OF CONTESTING CANDIDATES.
(See rule 10(1) of the Conduct of Elections Rules, 1961)
Election to the Tamil Nadu Legislative Assembly
From the 150.Jayankondam Assembly Constituency
SI.

Name of candidate

(1)

(2)

No

Candidate’s photo | Address of candidate.

(3)

Party

Symbol Allotted

(5)

(6)

affiliation.

(4)

(i) Candidates of recognised National and State Political parties.

te

Ze

3)

4.

Kannan,

Ka.So.Ka.

Neelamegam, K.

237, Sengal Oda
Theru,
Ko.Kandiyankollai
Village,
Kodalikaruppur Post,
Udayarpalayam Taluk,
Ariyalur District,
Pin: 612902.

25/10B, North Street,
Udayarpalayam,
Udayarpalayam Taluk, |
Ariyalur Disirict,
Pin:621804.

(ii} Candidates of registered Political Parties.
(other than recognised National and State Political parties)

Siva, J.K.

Sornalatha @
Latha, G.

Dravida
Munnetra
Kazhagam

90/248B, 2™ Street,

Velayutha nagar,
Jayankondam,
Jayankondam Post,
Udayarpalayam Taluk,
Ariyalur District ,
Pin: 621802.

2/15A, South Street,
P.Kaduvetti Village,
Padanilai Post,
Udayarpalayam Taluk,
Ariyalur District ,
Pin: 612903.

Bahujan
Samaj
Party

| Amma

Makkal
Munnetra
Kazhagam

Indhiya
Jananayaga
Katchi

Rising Sun

Elephant

Pressure

Cooker

AutoRickshaw

5:

Natarajan, A.

4/135, Melatheru,
Ayuthakalam South
Village,
Ayuthakalam Post,
Udayarpalayam
Taluk,

Anna
Dravidar

Hat

Kazhagam

Ariyalur District,

Pin: 612901.

6.

Balu, K.

ih:

Mahalingam,
Neela.

No.193,
Era. Vazhakuttai
Village,
Kundaveli Post,
Udayarpalayam Taluk,
Ariyalur District,
Pin: 612903.

Pattali
Makkal
Katchi

Mango

No.932B,

Naam
Thamizhar
Katchi

Ganna
Kisan

Sannathi Street,
Nayaganaipriyal

Village,
Nayaganaipriyal Post,
Udayarpalayam Taluk,
Ariyalur District.
Pin: 621804.
(iii) Other candidates.
Kesavarajan, V.K.

4819, TNHB,
Ayappakkam,

Independent

Jackfruit

125C, Kumbakonam
Salai,
Velayutha Nagar,
Jayankondam,
Jayankondam Post,
Udayarpalayam Taluk,
Ariyalur District,
Pin :621802.

Independent

Bucket

828,Madathu Street,
Koovathur Village,

Independent

Tractor
Chalata
Kisan

Ambattur Taluk,
Thiruvallur District,

Pin:600077.

9.

10.

Sathishkumar, R.

| Samuvel Martin, A.

Koovathur Post,
Andimadam Taluk,

Ariyalur District,
Pin:621803.

16B, Kambar Sireet,
Sengunthapuram,
Sengunthapuram
Post,
Udayarpalayam Taluk,

11. | Sudarvizhi, K.

Independent

Gas Stove

Independent

Cauliflower

Independent

Water
Melon

_ | Ariyalur District,

Pin:621802.

12. | Sedhuraman,

R.

No.191, Nadu Theru,
Thirukalappur Village,
Thirukalappur Post,
Andimadam Taluk,

Ariyalur District,

Pin:621805.

13. | Rajkumar, S.

13/9A, Indira Nagar,
Jayankondam,
Jayankondam

Post,

Udayarpalayam Taluk,

Ariyalur District,

Pin: 621802.

Place :Udayarpalayam.
Date :22.03.2021.

Se aie

aN

Q

)

(R. AMARNATEV
Returning Officer,
150.Jayankondam Assembly Constituency,
and Revenue Divisionai Officer,
Udayarpalayam.

isional Offi

dayarpalayam, oticet

