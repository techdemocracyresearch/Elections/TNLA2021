FORM 7A
LIST OF CONTESTING CANDIDATES
[See rule 10(1) of the Conduct of Elections Rules, 1961]
Election to the Tamil Nadu Legislative Assembly
From the 110 Coonoor Assembly Constituency

Sl.
No.

Name of Candidate

Candidate’s photo

(1)

(2)

(3)

(i)

Candidates

Address of
candidate

(4)

Symbol
allotted

All India
Trinamool
Congress

Flowers &
Grass

(S)

(6)

of recognized National and State Political Parties

D.No.34,
Narmadha

1. | PRABHU

Party
affiliation

INBADASS,

S.

Apartment,
Sheshasthiripuram,
Varadharajapuram,
Mudichoor,

Kanchipuram600 045

Bee 149/ oe

2. | RAMACHANDRAN, K.

elvaganapathy

FF

Illam

Bettatti Sungam,

Yedappalli Post,
Coonoor-643 104.
The Nilgiris.

D.No.2/261,

3. | VINOTH,

D.

Kappachi,
Thummanatty

Dravida

Se aan
§

at

sing

Sun

All India

Post,

Udhagamandalam.
The Nilgiris.

Anna
Dravida

Mitiinetra,

Kazhagam

Two

Leaves

(ii)

Candidates

of registered Political Parties

(other than recognized National and State Political Parties)

ARUMUGAM,

D.No.49,

Anaithu

Alwarpet,
Coonoor-643

Puratchi
Katchi

Samayapuram,

R.

101

The Nilgiris.

D.No.1/359,

Currency Bazaar,
Currency Post,
Coonoor-643 102
The Nilgiris.

KALAISELVAN, S.

RAJA KUMAR,

H.B.

( er
Samet

ae

:

)

Makkal

Amma

Makkal
Munnetra
Kazhagam

D.No. 6/221,
Muttinadu Village,

Makkal

Coonoor-643 203

Maiam

Athigaratty Post,

The Nilgiris.

Needhi

Glass

Tumbler

Pressure
Cooker

Battery
Torch

D.No.21-A,

Vijayalakshmi

LAVANYA, M.

Naber
Neelikonam-

palayam
Post,-641 033
Singanallur,
Coimbatore

South.

Naam

Tamilar
Katchi

Ganna

Kisan

(iii)

Other

Candidates.

D.No.5/93A,
Asirvadh Compound,
8. | CHANDRAN,

9. | BASHA,

Bedford,
Coonoor-643

K.

The Nilgiris.

A.

10. | JAYAPRAKASH,

D.

Independent

D.No.3/128,
Yedappalli Village,
Yedappalli Post,

Independent

Coonoor-643

Coonoor

22-03-2021

Independent

DANO Ie
Cornwall Road,
Sims Park,
Coonoor-643 101
The Nilgiris.

Signature
Name

Pot

Gas

Cylinder

Diamond

104

The Nilgiris.

Place :
Date :

101

:

We

RANJEET

SINGH

Returning Officer,
110 -Coonoor Assembly Constituency &
Sub Collector, Coonoor.

NB:- Under column (1) above the serial numbers of candidates of all the three
categories shall be given consecutively and not separately for each category.

