FORM 7-A
LIST OF CONTESTING

CANDIDATES

[See rule 10(1) of the Conduct of Election Rules, 1961]
Election to the Tamil Nadu Legislative Assembly
From the 120 Coimbatore South Assembly Constituency
Serial |

Name of the

Candidate’s | Address of Candidate

No

Candidate

Photo

()

(2)

(3)

(4)

(i) Candidates of recognised National and State Political Parties.
1

2

3

Mayura

Jayakumar .S

Roshan $.S.S

Vanathi
Srinivasan

Party

Symbol allotted

Affiliation

43-A, K.R.K Street,

Krishnasamy Nagar,

6)

(6)

Indian National

HAND

Bahujan

ELEPHANT

Bharatiya
Janata Party

LOTUS

Congress

Ramanathapuram,
Coimbatore.
Pincode - 641 045

No. 423/R 9,

V K K Menon Road,
Housing Unit,
Siddhapudur,
Coimbatore.
Pincode - 641 044
Sree Vatsa Shiva
Apartment, 304,
Fourth Floor,

Samaj Party

Dr. Rajendra Prasad

Road, 100 Feet Road,
Tatabad,
Coimbatore.
Pincode - 641 012

Abd

RETUR We OFFICER
120- COIMBATORE SOUTH
ASSEMBLY CONSTITUENCY AND
ASSISTANT COMMISSIONER
( CENTRAL ZONE )

COIMBATORE CORPORATION

-2-

Serial
No

()

Name of the
Candidate

Candidate’s
Photo

(2)

Address of Candidate

G3)

(4)

ii) Candidates of registered Political Parties.

Party
Affiliation

Symbol
allotted

Naam
Tamilar

GANNA

(5)

(©)

(other than recognised National and State Political Parties)
4

Abdul Wahab.A

4-1/8, Asma Illam, Valliyamai

Nagar, Selvapuram,
Coimbatore.

Pincode - 641 026

5

Kamal Haasan

Katchi

New No. 4, Old No.172,
Vanniya Teynampet,

Alwarpet,
Chennai.

Gopalakrishnan.

M

5/ 481-4, Namitha Garden,

Kuppanaickenpalayam,

3 / 438, Indra Nagar,
Perur Chettipalayam Post,
Coimbatore.

9

Duraisami

3- 4/ 370 C, Balasundaram

@
Challenger Durai. R

Raghul Gandhi.

Layout, New Siddhapudur,
‘ PA
aah

oe

K

Vivek
Subramaniam.

M

5/14,
Central Excise Colony,
Rajaji Layout, Vadavalli,

not

es /

Vellimalai. S

ieel

LADY

y
India

FINGER

Kama
Makkal

PRESSURE

Munnettra

COOKER

Hindustan

BAT

67, Thadagam Salai,
Velandipalayam,

Manitha

TRACTOR

Pincode - 641 025

Urimaigal

CHALATA

Janata Party

Coimbatore.

o

1

KISAN

Kalaagam

ie
11

eto

Kazagam

Coimbatore.
Pincode - 641 041

10

POT

People's
Party

Pincode - 641 010

8

New

Generation

Coimbatore.
Pincode - 641 108
Shanmugavel. T

ee

Maiam

Kanuvai,

7

Y
PESOS

Makkal
Needhi

Pincode - 600 018
6

KISAN

8/4, Jayaprakash Narayanan
Street,

Edaiyarpalayam,

Vellalore Post,
Coimbatore.
Pincode - 641 111

Makkal

Sananayaga

AUTO -

Kudiyarasu
Katchi

RICKSHAW

gn

RETURNINGIOPRICER
CAIRAD

ADS

EMIESL

ASSISTAI

ATOARE

SOUTH

MoODSDONEN

CENTRAL ZONE )

N
COIMBATORE CORPORATIO

ID

Sie

Serial
No

(1)

Name of the
Candidate

(2)

(iii) Other Candidate:
12
Alphonseraj. M

Candidate’s
Photo

(3)

Address of Candidate

(4)

14

K

Sundaravadivelu. N

16

Ue

18

19

Chelladurai. S

Selvakumar..K

Dhandapani.

B

Nagavalli

Palanikumar. V

Independent

HELMET

(6)

338, Shanmugam Street,
Ramanathapuram,
Coimbatore.
Pincode- 641 045.

Independent | CAPSICUM

180 /128,

Tirugnana sambandar Road,
Race Course, Coimbatore.
Pincode- 641 018

15

allotted

Old.No:186,
New.No:87E, Sastri Nagar,
Coimbatore.
Pincode- 641 012.

Kumaresan.

Symbol

(5)

Sathy Road, Gandhipuram,

13

Party
Affiliation

115/ 9, Rajiv Nagar,
Amman Kulam,
Coimbatore.
Pincode- 641 045

Independent

97, Ammankulam Road,
Papanaickenpalayam,
Coimbatore.
Pincode- 641 037

563 ,
7th Street Extension ,
Gandhipuram,
Coimbatore.
Pincode- 641 012

873, Salivan Street,
Coimbatore.
Pincode- 641 001

38/3, Telugu Street,
Coimbatore.
Pincode- 641 001

Independent | TRIANGLE

VIOLIN

Independent | FOOTBALL

GAS

Independent

CYLINDER

Independent

RING

Independent

FLUTE

tba

RETURNIN

FFICER

420- COIMBATORE SOUTH
ASSEMBLY CONSTITUENCY AND
ASSISTANT COMMISSIONER

( CENTRAL ZONE )

COIMBATORE CORPORATION

Al

Serial
No

(i)
20

Name of the
Candidate

(2)

Jayachandran. S

Candidate’s
Photo

(3)

Address of Candidate

(G))

19, Balu Garden,
Opposite to FCI Road,

Ganapathy,
Coimbatore.

Party
Affiliation

Symbol allotted

(5)

(6)

n
Independent | T.V. REMOTE

Pincode- 641 004
Pai

Jayaprakash.

N

2/ 108,
Kondegoundanpalayam
post, Negamam via,
Pollachi Taluk

Independent

HAND

Pincode- 642 120

Place : Coimbatore
Date : 22.03.2021

m\
Ts SIVASUBRA
ANIAN
Returning Officer,
120 Coimbatore South Assembly Constituency
and Assistant Commissioner, Central zone,

Coimbatore Corporation.

CART

