FORM 7A
LIST OF CONTESTING CANDIDATES
[See rule 10(1) of the Conduct of Elections Rules, 1961]

Election to the Tamil Nadu Legislative Assembly
From
Sl.
No.

»@)

Name of Candidate,

(2)

the 073, Vanur (SC) Assembly Constituency

Candidate’s Photo

Address of Candidate

Party affiliation

(4)

(5)

(3)

Symbol

allotted

(6)

(i) Candidates of recognised National and State Political Parties

GANAPATHI

Fa

P M

CHAKRAPANI

M

VINAYAGAMURTHY

(II)

M

Candidates Of Registered Political Parties

NO.872,
GOPALAKRISHNA
NAGAR,
THIRUCHITRAMBALAM,
VANUR TALUK,
VILUPPURAM
DISTRICT - 605 111

_DESIYA
MURPOKKU
DRAVIDA
KAZHAGAM

NAGARA

NO.176,
INDIRA NAGAR,
KILIYANUR & POST,
VANUR TALUK,
VILUPPURAM
DISTRICT - 604 102.

ALL INDIA ANNA
DRAVIDA
MUNNETRA
KAZHAGAM

TWO
LEAVES

NO.2/374,
KUDIMARA STREET,
PERIYA BABU
SAMUTHIRAM,
VIKIRAVANDI TALUK
VILUPPURAM
DISTRICT - 605 102

BAHUJAN SAMAJ
PARTY

ELEPHANT

NO.3/69,
AMBEDKAR NAGAR,
MORATTANDI,
THIURCHITRAMBALAM,
AROVILLE POST,
VANUR TALUK
VILUPPURAM
DISTRICT - 605 101.

MAKKAL NEEDHI
MAIAM

BATTERY
TORCH

NO. 130,
PUDHU COLONY,
PERAMBAI,
VANUR TALUK,
VILUPPURAM
DISTRICT - 605 110

NAAM TAMILAR
KATCHI

GANNA
KISAN

(Other Than Recognised National And State Political Parties)

SANDOSHKUMAR

LATCHOUMY

M

M

VANNI ARASU
a

NO.06,
PILAYAR KOIL MAIN
ROAD,
DARKASH,
VARADHARAJAPURAM,
KUNDRATHUR TALUK
KANCHEEPURAM
DISTRICT -600044

VIDUTHALAI
CHIRUTHAIGAL
KATCHI

(ii) Other Candidates
f

SAKTHIVEL S

Place
Date

-Vanur.
22 .03.2021

NO.55,
INDIRA NAGAR,
KILIYANUR & POST,
VANUR TALUK,
VILUPPURAM
DISTRICT- 604 102.

INDEPENDENT

(S.SIVA)
Returning Officer;
073.Vanur (SC) Assembly Con
stitueri

Assistant Commissi
oner (Excise),

Vi luppuram,

