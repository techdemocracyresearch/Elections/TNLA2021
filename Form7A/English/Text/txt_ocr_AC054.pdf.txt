FORM

7A

LIST OF CONTESTING

CANDIDATES

[See rule 10 (1) of the Conduct of Elections Rules, 1961]
Election to the Tamilnadu Legislative Assembly .
From the 54.Veppanahalli Assembly Constituency.

Si.
ee

(1)

:
.
:
Name of Candidate | Candidate's: Photo

Address of
Candidate

Party
affiliation

Symbol
allotted

(4)

(5)

(6)

(3)

(2)

(i) Candidates of recognised National and State Political Parties.

1

2/227-A,
Thalavaiapalli village,
Samandamalai Post,
:

P.Murugan

Krishnagiri Taluk.

Krishnagiri District.

635 115.
27s
2

S.M .Murugesan

aa
=~

/

Kulalar Street,
Rayakottai Village
and Post,
Denkanikottai Taluk,

Krishnagiri District.
635 116.

=

Dravida

Rising Sun

Munnetra

Karna

Aenean

Teste
Nagara

es
ae

ae

aes

Old No.133A,

eet
3

K.P. Munusamy

C;

—

New No.1,

.

Jinna Street,

eee

aes
3
Krishnagiri Taluk,

Munnetra

Kaveripattinam,

Krishnagiri District

ae

Two Leaves

Kazhagam

635 112.

(ii) Candidates of registered Political Parties.
(Other than recognized National and State Political Parties)
4/301,
Kottai

2™ Street,

Rayakottai Village

Sakthivel. M

and Post,
Denkanaikottai Taluk,

Krishnagiri District.

635 116.

Naam

Tamilar

Katchi

Ganna Kisan

1/216-A,
Nallaralapalli Village,
Haleseebam Post,

Thangapandian. S

Ayarnapalli

Panchayat,
Shoolagiri Taluk,

Tamilnadu
llangyar
Katchi

Ring

Krishnagiri District .
635 119.
2/115,
V.Madepalli
Mariyappan.M

Valli.M

and Post,

Village

Krishnagiri Taluk,
Krishnagiri District.
635 121.

Door No.1,
Pookara Theru,

Palayapettai,
Krishnagiri.
635 001.

Door No.2/465,
Perandapalli

Panchayat,
Jayapal.P

Bathalapalli Village,
Hosur Taluk,

Puthiya
Tamilagam

Television

Anna Puratchi
Thalaivar
Amma

Dravida

Munnetra

Jackfruit

- Kazhagam

Makkal

Needhi

Maiam

Battery Torch

Krishnagiri District.
635 109.
(iii) Other Candidates

Abdul Azeez
Amanullah

No.3/468/3-94,
Athimugam Post.
Shoolagiri Taluk.

Krishnagiri District.
635 105.

Door No.2/237,
Gudisetlu Village,

10

Usha.J

Independent

Thummanapalli Post,
Hosur Taluk,

Krishnagiri District.
635 105.

Independent

Tractor

Chalata Kisan

Grapes

1/49,
Krishnapalayam,

Krishnamoorthy.P

Shoolagiri (Post),
Shoolagiri Taluk,

Independent

Well

Krishnagiri District635 ull”,
Door No.457,

Gundukurukki
Village,

Chakkarlappa. C

Koneripalli Post,
Shoolagiri Taluk,
Krishnagiri District.
6357.

Independent

Bat

Independent

Pan

1/425-9,
Sathyasai Nagar,

Yuvaraj.K.P.U

TNHB II nd Phase,
Krishnagiri .

635 002.

3/106,

14

Govinda agraharam
Village,
Ramasami.G.C

Independent

Begepalli Post,

Hosur Taluk,
Krishnagiri District.

Air

Conditioner

635 126.

15

Lakshmanan
Shanmugam

Place

: Shoolagiri.

Date

=: 22.03.2021

1155,
Chinnasappadi
Sappadi Post,

Independent

Shoolagiri Taluk,

Krishnagiri District
635 117.

We

Bread

oye

G.GOPU
RETURNING OFFICER,
54.VEPPANAHALLI ASSEMBLY CONSTITUENCY AND
DISTRICT SUPPLY & CONSUMER PROTECTION OFFICER,
KRISHNAGIRI.

