FROM 7-A
LIST OF CONTESTING CANDIDATES

[See rule 10(1) of the Conduct of Elections Rules, 1961]

From the 8&Thiruporur Assembly Constituency.

Serial
Number

(1)

Name of the
candidate

(2)

Candidate Photos

Address of candidate

(3)

Party
affiliated

Symbol allotted

(4)

(5)

(i) Candidates of recognized National and State Political parties

No.2/82, Ganthi

1

Street, Bharathi
Nagar,
d
Mettuthandalam,

is
Pakkiri

Ambadkar.K.V.

Thirtporneaine

:
Bahujan

Samaj Party

Elephant

Chengalpattu
District-603110
(ii) Candidates of registered Political Parties

(other than recognized National and State Political Parties)

No.36,

Sriperumbudur Salai,

2

Arumugam.K

Thirukkachur,

Maraimalai Nagar
Municipality,
Chengalpattu Taluk &

Pattali

Makkal
Katchi

Whine

8

District.

No.19/A, Othavadai

3

:
Kothandapani.M

Sis

Mamallapuram,

Balaji.S.S

Makkal

TEN TU le ieaenulednidbaml(Mianetre

Taluk, Chengalpattu.
District-603104

4

Amma

New No.21,
Old No.11,

3rd Mainroad, CIT
Nagar, Chennai

District-600035

Pressure Cooker

| Kazagam

Viduthalai

Chiruthaigal
Katchi

Pot

5

6

Mohanasundari.S

Lavanya.N

No.76, Mangaleri,
Thiruvanmiyur,
Chennai District600041

No.3A, Krishna
Accolade,
Jeyachandra Nagar,
Ist Main Road,

».| Pallikarani, Chennai

Naam
Tamilar
Katchi

Makkal
Needhi
Maiam

Ganna Kisan

Battery Torch

District-600100

(iii)Other candidates

q

Akbar Bhasha.U

No34, Ist cross Street,
Periyar Nagar,
Thirukkachur,

Maraimalai Nagar,
Chengalpattu Taluk &

Independent

Hat

Independent

Biscuit

Independent

Air Conditioner

District

8

Duraisamy.M

No.353,
Sengazhuneerodai
Street, Manamathi
Thiruporur Taluk,
Chengalpattu
District-603105

9

Nataraj.S

No.494/NA,
Sengazhuneerodai
Street, Manamathi
Thiruporur Taluk,
Chengalpattu
District-603105

No.19E/63 Valmigi
| Street (Mullai Street),
10

| Thiruvalluvar Nagar,

| Perungudi,

Ravi.D

a

11

_|

600096

No.51, 3rd Mullai
Street, Thiruvalluvar
Nagar, Perungudi,

Joshva.A

Independent | Auto-Rickshaw

Sholinganallur Taluk,
Chennai District-

Sholinganallur Taluk,

Independent

Chennai District600096

Place; Thiruporur
Date; 22.03.2021
9:

e

SS

VX
'

De

U

eee tees

Wn

Relea

oe

“A

Officer,

No.33.Thiruporur Assembly Constituency and

Additional Personal Assistant to Collector(Land),
Chengalpattu.

NB----Under column (1) above the serial numbers of candidates of all the three categories
shall be consecutively and not separately for each category.

Swing

