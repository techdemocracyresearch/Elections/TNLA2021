FORM 7A
LIST OF CONTESTING CANDIDATES

[See rule 10(1) of the Conduct of Elections Rules, 1961]
Election to the Tamil Nadu Legislative Assembly
From the No.23.Saidapet Assembly Constituency
Party

No

Si

Name of
candidate

Candidate’s
Photo

Address of
candidate

affiliation

(1)

(2)

(3)

(4)

(S)

Symbol allotted

(6)

(i) Candidates of recognised Nationa | and State Political parties

No.9/16,
Thiruvalluvar
nagar,
Ath street,
Kottur,

1 | Kottur Kumar.
R

|

No.7/4,
Labour Colony,
Guindy,
Chennai-32

Duraisamy

ee

|

_ | No.28/62,

a

Elephant

Dravida
Munnetra
Kazhagam

Rising Sun

All India Anna

Two Leaves

| Chennai-85

2 | Subramanian.
Ma

3 | Saidai

Bahujan
Samaj Party

|

1st Main Road,

| | CIT Nagar,

__| Chennai-35.

a

Dravida

Munnetra

Kazhagam

(ii) Candidates of registered Political Parties
(other than recognised National and State Political parties)
4 | Sagayamary.N

No.30/34,
St.Thomas
Nagar,
Little Mount,
Saidapet,
Chennai-15.

Republican
Party of India
(Athawale)

Helicopter

5 | Sivagnanasam
bandan.T

No.23,
Hospital Road,
'J'Apartment,
Kaveri Nagar,
T.Nagar,
Chennai-17

Desiya
Makkal
Sakthi Katchi

Football

‘

Snegapriya

No.13/1,
Anna Street,
S.P.Garden,

Makkal
Needhi
Maiam

Battery Torch

Naam
Tamilar
Katchi

Ganna Kisan

Amma
Makkai
Munnettra

Pressure Cooker

Camera

T.Nagar,

Chennai-17.

Suresh Kumar.
B

No.6/17,

Kattabomman
Street,

Ambal Nagar,

Ekkattuthangal,
Chennai-32.

Senthamizhan.
G

No.53,
Sasthri Street,
Kaveri Nagar,
Saidapet,
Chenai-15.

Kazagam

(iii) Other Candidates

g

Ashok Kumar.V

No.8, Subramani
Street,
Muthurangam
Block,
Jafferkhanpet,
Chennai-83.

Independent

10

Arun Kumar.M

No.5, 'U' Block,
Tamil Nadu Slum
Clearance Board
Quarters,
Bharathidasan
Street, Kaveri

Independent

Box

Nagar, Saidapet,

Chennai-15

11

Alaganathan.B

No.30/1,

Independent

Calculator

Independent

Hat

Madha Church
Lane,
Bazaar road,

Saidapet,

Chennai-15.

12

Imran Khan.M

No.1, Chinnaiah
Garden,
2nd Street, East
Jones Road,
Saidapet,
Chennai-15

13

Elango.K

No. 12,
Appadurai Street,
Jafferkhanpet,
Chennai -83

Independent

Truck

14

Kandhasamy.R

No.56, Periya
Karumariyamman

Independent

Ring

Independent

Kettle

Kovil Street,

Gandhi
Mandapam Salai,
Kotturpuram,

Chennai-85.
15

Kothandapani.
K

No.10,

V.S.M.Garden
Street,

Jafferkhanpet,
Chennai -83

16

Sathish Kumar.
S

No.6-1/20,
Rajambal Street,
Sekar Nagar,
Jafferkhanpet,
Chennai-83

Independent

Ship

lit

Saravanan.D

No.40/14B,
Ragava Reddy
Colony 4th

Independent

Mike

Independent

Television

Street,
Jafferkhanpet,

Chennai-83

18

Sivasankar.S.R

19

Balaji.V

No.9/2,
Mani Street,
Sarathi Nagar,
Saidapet,
Chennai-15.

No.14/10,

Karani Thottam
3rd Street,
Saidapet,
Chennai-15

Independent

Pen Stand

20

Prakash.C

No.141/2A,
Ragava Reddy
Colony 4th

Independent

Window

Street,
Ashok Nagar,
Chennai-83

21

Prem Kumar.A

No.16/39,
Sasthri Nagar
1st Cross Street,
Kaveri Nagar,
Saidapet,
Chennai-15

Independent

Gift Box

22

Manimaran.S

No.34/18,
Padavettamman
Kovil 1st Street,
West Mambalam,
Chennai -33

Independent

Bread

23

Murthi.R

No.8/20,
C.P.W.D.
Quarters,

Independent

Diamond

K.K.Nagar,

Chennai-78

24

Rajesh.N

25

Rishi Kumar.A

26

Loganathan.J

Independent

Door Bell

No.14/11,
Mosque Garden,
East Jones
Road,
Saidapet,
Chennai-15

Independent

Rubber Stamp

No.19,
Rajabadhar
Street,
Jothiramalingam
Nagar,
Jafferknanpet,
Ashok Nagar,
Chennai-83

Independent

Matchbox

No.32,
Devaraj Street,
Jothi
Ramalingam
Nagar,
West Saidapet,
\| Chennai-83

27

Vasanth.H

No.2/11,

S.P.Garden,

Independent

Flute

T.Nagar,

Chennai-17

28

Vijaya Kumar.R

| No.2,

Mettu street,
Poonthamalle
Road,

Independent

Helmet

Ekkattuthangal,

Chennai-32

29

Venkatesh.K

No.82,
Chetti Thottam,

Independent

Water Melon

Saidapet,

Chennai-15

30

Jothi Sairam.S

No.80/54-3,
V.S.Mudali
Street,

Independent

Pen Drive

Saidapet,

Chennai - 15

Place : Chennai.
Date

: 22.03.2021.

ds why S242 R

( Dr.V.ALIN SUNEJA

RETURNING OFFICER,
NO.23, SAIDAPET ASSEMBLY CONSTITUENCY
AND DISTRICT REVENUE OFFICER/
SECRETARY, TAMIL NADU SLUM CLEARANCE
BOARD, CHENNAI-600 005.

