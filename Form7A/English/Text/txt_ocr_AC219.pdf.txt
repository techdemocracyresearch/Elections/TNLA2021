FORM 7A
LIST OF CONTESTING

CANDIDATES

[See rule 10(1) of the Conduct of Elections Rules, 1961]
Election to the Tamil Nadu Legislative Assembly
From the 219-Sankarankovil (SC) Assembly Constituency
e

Name of Candidate

(1)

(2)

Re

Address of Candidate

Party Affiliation

(4)

(5)

(3)

(1) Candidates of recognised National and State Political Parties
gam,
Be

:
4
lakshmi, V.M.
elateksharnl

2 | Raja, E.

(ii) Candidates of registered pol

Facm|
Sat
on

ee =

rien

35,Gandhinagar East 3

Street
:
:
Sankarankovil,
Tenkasi District.

250, Amman

Kovil Street,

Chinthamani, Tenkasi

Taluk. Tenkasi District.

Dravida
Munnetra
Kazhagam

Dravida

Munnetra

Kazhagam

(6)

Two
Leaves

Rising Sun

r than recognised National and State political parties)
3/205, Church Street,

3 | Annadurai, R.

All India Anna

a

Black Sas

olay

Thiruvengadam Taluk,
Tenkasi District.

Amma

Makkal

Munnetra

Pressure

Ai

Television

aera
g

Cooker

165/4, Narayanapuram

4

Subramaniam, V.

5 | Panneerselvam, U.

Street,

Keezhaneelithanallur,
Sankarankovil Taluk,
Tenkasi District.

se
g

401, Johnkennady Street,
Sathyamoorthy Block
Y
Y
e

4
ammiehaga
Murpokku

West Jaffarkhanpet,
Chennai.

Makkal Katchi

Matchbox

167/21A, South Colony,
OS

|

Bala digesta)

.

Sat

-

-

Prabhu,

eee

Re
we
Yea

K.

Nay
F

Sankarankovil, Tenkasi

Bahujan Dravida
Party

340B, Bharathi Nagar,
Duraiyur, Gangaikondan,
2
; Taluk,
Tirunelveli

Makkal Needhi
i
Maiam

Railway Feeder Road,

District.

Tirunelveli District.

SUE!

Battery

Torch

10/504A, North Colony,
8 | Mahendrakumari, P.

Je

blaunan sunt,

K.

Nom
Azhagunatchiapuram,
.
Thiruvengadam Taluk, Tenkasi District.
378, Railway Feeder
Road, South Colony,
Achampatti, Sankarankovil
Taluk, Tenkasi District.

Naam Tamilar
‘
Katchi

Selle
Kisan

:
My lndlenPanty

CCTV
Camera

(iii) Other Candidates

10 | Ganesan, A.

116, Dr.Ambedkar 2"

Street, Sankarankovil,
Tenkasi District.

Independent

|

Electric Pole

310,Palayapalayam

11 | Karuthapandiyan, R.

A a

st

SUS:

Rajapalayam,
Virudhunagar District.

Independent

eae

oe

12 | Gururaj, P.

13 | Muthukkutti, S.

Pe

;
Vall
|, M.
seen Mel:

25/10A, Gandhinagar East
1* Street, Sankarankovil,
Tenkasi District.

27, Gandhi nagar 6"

Street, Sankarankovil,
Tenkasi District.

225, Karuppasamy Kovil
Street, Pudugramam
A
'
Devarkulam, Tirunelveli
District.

Independent

Independent

Diamond

Pen Nib
with Seven
Rays

i
Munisile

ng Pelsent

4/81, South Street, Jamin
Devarkulam, Kovilpatti

15 | Vetrimaran, A.

Taluk, Thoothukudi

District.

Independent

Gas

Cylinder

qn

os

vy”
Place:

Sankarankovil

Date:

22.03.2021

M.MURUGASELVI
Returning Officer
219.- Sankarankovil (SC) Assembly Constituency
and Revenue Divisional Officer

Be
29-

Sankarankovil

