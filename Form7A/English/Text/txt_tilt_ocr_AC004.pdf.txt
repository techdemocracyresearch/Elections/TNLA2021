FORM -7A
LIST OF CONTESTING

CANDIDATES

(See rule 10(1) of the Conduct of Elections Rules, 1961)

Election to the Tamil Nadu Legislative Assembly From the 004. Tiruvallur Assembly Constituency]

SI. No.

Name of Candidate

1

2

02

RAAJENDRAN, V.G.

DOSS,

Address of Candidate

3
;

of

| Candidate’s Photo

D.

(I)

CANDIDATES OF

4
RECOGNISED

NATIONAL

AND STATE

No. 1, V.G.R.Garden,

Partly affiliation

Symbol allotted

5

6

POLITICAL PARTIES

V.G.R.Nagar, Pandur,

Sesiueees

Rising Sun

No.138, T. Pudur Road,
Jothi Nagar,
Tiruttani -631209

Bahujan Samaj Party

Elephant

Tiruvallur Taluk - 631203

gal

03

No.12,
V.M.Nagar Annex,

RAMANAH, BE VEE

V.M.Nagar, Tiruvallur

Town, Tiruvallur Taluk &
District. 602001.

MERCH

rae

Two Leaves

9

|

a
(ll) CANDIDATES OF REGISTERED POLITICAL PARTIES
(OTHER THAN RECOGNISED NATIONAL AND STATE POLITICAL PARTIES)

No.923, Gengaiamman
Kovil Street,

cai

Arunthadhipalayam,
Nadukuthagai,

SUSU!

Thirunindravur,

Amma Makkal
Munnettra Kazagam

Fp

Jeln Mises)

Tiruvallur - 602024

05

PASUPATHY.

P

No.1, Kilnallathur Road,
Ellaiyamman Nagar,
Kilnallathur, Tiruvallur
District — 602 002.

Naam Tamilar Katchi
Ganna

Kisan

(iii) OTHER CANDIDATES

06

07

KUMAR, E.

SASIKUMAR, J.

43/508, Ingar Sal Street,
Rajajipuram, Tiruvallur
District. 602001.

No.4/80, Government
Hospital Street,
Old Venmanampudur,
Kadambathur Post,

Independent

Independent

Tiruvallur Taluk & District.

08

BALA KRISHNAN, N.

Binoculars

Cauliflower

No.10 Perumbakkam
Tiruvallur District -

602001

Independent
Peas

09

RAMANAN, R.

No.422, Ambethkar

Street, Melnellathur

Village & Post, Tiruvallur
District — 602 002

10

RAJENDIRAN, G.

11

REVATHI, R.

Independent
Truck

No.319, M.P.S. Street,
Melnallathur, Tiruvallur
Taluk & District 602 002,

Independent

No.43/45, Lankakara
Street, Tiruvallur 602001

Independent

Diamond

Whistle

Date : 22.03.2021

Place : Taluk Office, Tiruvallur

fy
nd
fetid
Sn.

(oP

004. Tiruvallur Assembly
onstituency
and District Backward Classes and
Minority Welfare Officer, Tiruvallur.

NB- Under column (1) above the serial numbers
separately for each category.

NADY USD DEANAN)

of candidates of all the three categories shall be given consecu
tively and not

