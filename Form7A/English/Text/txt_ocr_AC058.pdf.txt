FORM 7-A
LIST OF CONTESTING CANDIDATES
[ See rule 10(1) of the Conduct of Election Rules, 1961]
Election to the Tamil Nadu Legislative Assembly 2021
From the 058-Pennagaram Assembly Constituency.
;
SLNo | Name of the Candidate

(1)

(2)

Candidate’s
Photo

s
Address of Candidate

(3)

(4)

Party
Pffiliatone |

(5)

(i) Candidates of recognised National and State Political Parties.

1.

INBASEKARAN.

P.N.P.

Salaikullathirampatti,
Noolahalli Post,
Pennagaram Taluk,

:
Dravida
Munnetra

Dharmapuri District.
636813.

Kazhagam

D.No.158,
Anna Nesavalar Colony,

5

,

|UTHAYAKUMAR.R

$

="j|

Papparapatti Post,
e

Pennagaram Taluk,

oe
_| Dharmapuri District.

636809.

3.

MOORTHI.S

D.No.1/226,
Errabaiyanahalli Village &

Post,

Symbol
eallatted.

(6)

Rising
Sun

5

Desiya

Murpokku |
.

Nagara

Dravida
ashawain

Bani
ahujan

Samay Panty

Elephant

| Nallampalli Taluk,
| Dharmapuri District.
636813.
(ii) Candidates of registered Political Parties.
(other than Candidates of recognised National and State Political Parties)

4. | ANNADURAI. C
:

D.No.5/209,
Gettuhalli Village,

;
i pan

Nallampalli Taluk,
Dharmapuri District
636803
D.No.78,

a }
Katchi

Pangunatham Post,

:i ‘

Football

Bramanar Street,

a

TAMILAZHAGAN. R

A.Papparapatti Post

es
Pennagaram

Taluk,

Dharmapuri District,
636809
1 of3

New

Tamilar
Katchi

Ganna

Kisan

D.No.4-150-1/4-132,
Ajjanahalli Village & Post,
Pennagaram Taluk,
Dharmapuri District,

MANI. G.K

Pattali
Makkal

Mango

Katchi

636810
D.No.2/561,
Ward-2, PWD

Oddapatti,
A-Jettihalli Village & Post,
Dharmapuri District.
636705

SHAKILA. K

(1)

ANNADURAI. V

SIVALINGAM. N

10

11

12

DHARUMAN.

Kk

THIRUMURUGAN.

P

PERIYA NANJAPPAN. A

2 of3

Colony,

Other Candidates
| D.No.4/669A, Pudhu En,
Peyalmari Village,
Mangarai Post,
Pennagaram Taluk,
Dharmapuri District.
‘| 636813
D.No.2/46,
Gettur Village,
B.Agraharam Post,
Pennagaram Taluk,
Dharmapuri District,
636813.
D.No.2/197, Vasanthamalai,
Somenahalli Village & Post,
Nallampalli Taluk,
Dharmapuri District.
636803.
Old No.3/96, New
No.3/99.A,
Mallikkuttai Post,

Karimangalam Taluk,
Dharmapuri District.
635205.
‘| D.No.1/18A,
Savulukottai Village,
Begarahalli Post,
Karimangalam Taluk,
_| Dharmapuri District.
636808.

Makkal
Needhi
Maiam

Battery
Torch

Independent

Television

Independent

Table

Independent

CCTV
Camera

Independent

Cot

Independent

Water
Melon

13

MANI. K.G.

D.No.1/447,
Hogenakkal
Koothapadi
Pennagaram
Dharmapuri
636810.

Village,
Post,
Taluk,
District.

Independent

Grapes

D.No.389,
Sekkumedu ,

14 | MANI. P

15

MUNUSAMY.S

Place
Date

3 of 3

: Pennagaram
: 22-03-2021

a

52\2\21

Independent

Pennagaram Post & Taluk,
Dharmapuri District.
636810.
D.No.4/96,
Rajavur Colony,
Senganur Post,
Pennagaram Taluk,
Dharmapuri District.
636810.

2.

A.THAN

Independent

Sewing

Machine

Air
Conditioner

Pc

AL

RETURNING OFFICER,
058, PENNAGARAM ASSEMBLY CONSTITUENCY AND
ASSISTANT COMMISSIONER (EXCISE), DHARMAPURI
TELEPHONE( OFFICE): 04342-255636
MOBILE: 9385250967

