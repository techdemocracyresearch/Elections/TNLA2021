FORM

7-A

(See Rule

10(1)

LIST OF CONTESTING CANDIDATES
Election to the Tamil Nadu Legislative Assembly
From the 212.Mudhukulathur

Legislative Assembly

Constituency.
Symbol

S.N

Name of the

oO

(1)

Candidate’s

Address of

candidate

Party

candidate

affiliation

(2)

(3)

(4)

(i) CANDIDATES

OF

RECOGNIZED

NATIONAL

AND

STATE

allotted

(5)

POLITICAL

PARTIES

All India Anna
iS

KEERTHIKA

MUNIYASAMY

W/O. A.Muniyasamy
8/140, Keela
!

Pallivasal Street,

Dravida
Munnetra

Kazhagam

Two Leaves

Paramakudi

S/O.

2. | N. SIVANANDAM

Natarajan,

Bahujan

4/5, Appanendal,

Samaj

Party

Sambakulam post,

Elephant

Muthukulathur
Taluk.

S/O. P.Sami
/
ee

46/1,

3

R.S.

| RAJAKANNAPPAN

Senthamil

"

illai
eg

7
Nagar, T.Puthur Main
Ellas
.

Ecole ys aetna
630561

Dravida
Munnetra
Kazhagam

Dict

pai

ae

(ii) CANDIDATES
(OTHER

THAN

OF REGISTERED
RECOGNIZED

POLITICAL

NATIONAL AND

PARTIES.
STATE

POLITICAL PARTIES)

S/O. T.Arumuga
Pandi,
4

A. SARAVANA

* | KUMAR

3/7 WAG,

Naam

Thiraviyapuram,

;
Indiar

Tractor
Chains

Party

Kisan

Maravan Madam,
Tuticorin District.
S/O.

Chokkalingam,

9/234,
5

NAVAPANNEER

' | SELVAM

Kadarkarai

Salai,

Makkal Needhi

Battery

Maiam

Torch

Kannirajapuram
(Post),
Kadaladi Taluk.

6.

S/O.

K.

PANCHATCHARAM

Kamatchi pillai,

145/141,

Senponkudi (Post),

My

India

P

een a

CCTV

eerie

Mudhukulathur.

S/O.
K arantha malai
lai
iy
Ls

K. BOOMIRAJAN
YADAV

geese
oo 1,

.
:
.
Periya Nachi Veethi,

Tamilaka

Makkal

Mwe

:

Thannurimai
Katchi

Diamond

1
Puthiya
Tanata

_
Television

Uthankudi,
Madurai.
S/O.
8. | MALAISELVAM

Chandran,

Kumarakurichi,
Karumal Post,

Mudhukulathur

Taluk.

:

Se

th
Muhidever,

440/1,
9.

|M. MURUGAN

Bazaar

Street, Kamuthi

,

.

Main Road,

°

Amma Makkal
Munnetra

Pressure

Kazhagam

Cooker

Naam Tamilar

Ganna

Mudhukulathur.

10. | RAHMAD

NISHA

W/O. Rahumathula,
10
e fier
Mathina Nagar,
Sikkal,
Kadaladi Taluk.

Katchi

:

.
Kisan

S/O.
11.

Stephen,

Mudukulathur Road,

S.RAJA

4th Cross Street,
Paramakudi 623707.

(iii)

OTHER

Peoples Party

Grapes

of India Secular

CANDIDATES
S/O. Thanikodi

123

T.AZHAKUMALAI

dever,

KUMARAN

Arisikkuzhuthan,

Independent

Auto

-

Rickshaw

kamuthi Taluk.
S/O.

Samidoss,

1/114, East Street,
13.

IMMANUVELSEKAR

Melaramanathi,

Independent

Football

Independent

Ring

Kavadipatti Post,
Kamuthi Taluk.

S/O. Kandasamy,
Hie

85, Mariamman

K.RAMACHANDRAN

Kovil

Street, Sayalkudi,
Kadaladi Taluk.

S/O. M.Murugan,
440/1, Bazzar

15.

M. KARTHICK

Street, Kamuthi

Independent

main road,

Electric
Pole

Mudukulathur.
S/O. Krishnan,
3/10, Paththira
6:

Kaliyamman Kovil

K. SATHISH

Street,

Independent

Helmet

Independent

Gift Pack

Independent

Bat

Sayalkudi, Kadaladi
Taluk.
S/O.

ie

G.M.

14/594,

SENTHILMURUGAN

DEVA

SITHAM. I

Bazzar

my | Street,
“|

18.

Muthuvel,

Mudhukulathur.

S/O. Immanuvel,
| 6/52, Devendra
Nagar,
Mudhukulathur.

S/O.

Sethuraman,

Selvanayagapuram,
ih

S. PRABAKARAN

_| Venneervaikkal post,

Independent

Chakki

Independent

Peas

Mudhukulathur
Taluk.

S/O.

Kurunthar,

345, Sengarpadai,
20.

K. MURUGAN

Valanadu

Post,

Mudhukulathur
Taluk.
S/O.
Ze

P.MURUGAN

Poovalingam,

Selliyamman

Kovil

Gas

Independent

Street,

Cylinder

Mudhukulathur.
S/O. Ebinesar,
137/30,
22.

E. MOHAN

Nayakkar East

Independent

Gas

Stove

Street,
Mudhukulathur.
|S /O. Kuppamuthu,

1/53,Alanganoor
235

K. RAJAKUMAR

Independent

| (Post),

Water Tank

Mudhukulathur
Taluk.

Constituency

PP

and

District

Backward

and Minorities welfare officer,
Ramanathapuram.

oY

Classes

|S RN MBREA )

