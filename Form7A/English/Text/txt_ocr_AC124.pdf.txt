FORM 7A

LIST OF CONTESTING CANDIDATES
[See rule 10(1) of the Conduct of Elections Rules, 1961]

Election to the Tamil Nadu Legislative Assembly
From the 124. Valparai (SC) Assembly Constituency
SL
No.

Name of candidate

Candidate’s photo

Address of candidate

Party
affiliation

Symbol
allotted

(1)

(2)

(3)

(4)

(5)

(6)

(i) Candidates of recognised National and State Political Parties
1. | Amulkandasami, T.K.

2.

| Arumugham, M.

Door No:3-238,

All India

Jeeva Nagar,
Ottarpalayam,
Annur Taluk,
Coimbatore District. |

Anna
Dravida
Munnetra
Kazhagam

Old Door No.4-B-1,

Communist

New Door No.22/2,
Shri Nagar Colony,

| Party of India |

Street No.4,

Two Leaves

Ears of

Corn and
Sickle

Pappanaickenpudhur,
Coimbatore-641041.
3. | Murugaraj, M.S.

Desiya
40/5,
Murpokku
Balaji Nagar Road,
New Siddhapudhur, | Dravida
Coimbatore District. | Kazhagam

Nagara

\_A
PpuRAIshene

224

RETURNING OFFICER,
124. VALPARAI (SC) ASSEMBLY CONSTITUENCY
AND INSPECTION CELL OFFICER,
COIMBATORE.

a”

eA

(ii) Candidates of registered Political Parties.

(other than recognised National and State Political Parties.
4. | KohilaC.

19/139,

Periyar Nagar,
Urulikkal (Post),
Valparai,

Naam

Tamilar
Katchi

Ganna

Kisan

Coimbatore-642125.

5. | Senthilraj D.

13/35W,
Makkal
Aththappa
Gounden | Needhi
Pudhur Road,
Maiam
Irugur (Post),

Battery
Torch

Coimbatore -641103.

(iii) Other candidates.

6.

Rangasamy

Place : Anaimalai

Date

: 22.03.2021

T.M.

72.
Nethaji Road,
Seeranaikenpalayam,
Coimbatore-64 1007.

Independent | Scissors

(sae

5>\3P™
P.DURAISAMY,
RETURNING OFFICER,

124. VALPARAI (SC) ASSEMBLY CONSTITUENCY
AND INSPECTION CELL OFFICER,
COIMBATORE.

