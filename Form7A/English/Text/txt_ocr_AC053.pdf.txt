FORM

7A

LIST OF CONTESTING CANDIDATES
(See rule 10(1) of the Conduct of Elections Rules 1961)
Election to the Tamil Nadu Legislative Assembly
From the 53. Krishnagiri Constituency
S.

/

No

Name of Candidate

(1)

(2)

Candidate’s

Address of

Candidate

Party affiliation

eilered

(3)

(4)

(5)

(6)

Photo

ae

xe

Symbol

(i)Candidates of recognised National and State Political Parties

1

;

Ashokkumar.K

Chandramohan.K.M

‘| 5/191, Thiruvalluvar
Nagar, II Cross,
:
Pa

Krishnagiri.

Re

Baty os
Krishnagiri.

ae
3

4

Senguttuvan.T

Tamilselvan.S.

es

,

Ne

Beek

eon
euDe
r
Munnetra

Kazhagam

Nationalist

Congress Party

;
Two Leaves

Clock

Dravida
ger

ae

Post, Krishnagiri TK
& District.

Munnetra

echaoan
aye

3/32, Ward No.1,

M-Nadupatti Village | Bahujan Samaj
& Post, Bargur Taluk, | Party
Krishnagiri.

e

Rising Sun

5
Blepaat

(ii) Candidates of Registered Political Parties
(Other than recognised National and State Political Parties)

Ameenulla

20-A,Kumbarapettai,
Ward No.6 Hosur
Taluk, Krishnagiri
District.

| All India MajlisE-Ittehadul
Muslimeen

Kite

65/15 B, Jakkappan

Sasikumar.K.S.

Nagar, 8" Cross,
Krishnagiri .

6

7

Nirandari.V

’
Be

5 ee

SN
P|

s

Nog

|

4

8

Achamangalam Post,

Bargur Taluk,
£ | Krishnagiri District.

Thalaivar Amma

Jack Fruit

Dravida
Munnetra
Kazhagam

| Naam Tamilar

Katchi

Gannalkaisan

&

Taree
cs

Ravishankar.R.K

1, Nagamangalam
Village,

Anna Puratchi

on
\ ‘ !
~~!

2/3, ; 34 Cross, : Mohan
Rao Colony,
?
fos
Krishnagiri.

pees
Matam

sdhi

eo

Batterv y TTorch

¥

9

10

Ruthramani.T

Vijayakumar.R

oat Peg

aa

pest eMastal
Sakthi
Katchi

Fo t

4/54, Mottur Village

sere

Pestle and

Taluk

Thozhilalarkal

sea
Krishnagiri.

& Post, Krishnagiri

oe
i

;

IS Sd

oohaeBall

Mortar

Katchi

(iii) Other Candidates

11

TVS Gandhi

: acl: |
«tay
I|

MIG-8, Housing

Board, Kattiganapalli, | Independent
Krishnagiri.

T.V.Remote

3/350, Subedhar Medu

12

| Kumaresan.M

vee

Kattinayanapalli Post,
Krishnagiri Tk & Dt.

Independent

Road Roller

13

t

Gopinath.M

1/63, IInd

_|

/

Cross,

Mullai Nagar,
Krishangiri.

;

Independent

Dish Antenna

Independent

Computer

‘Lk

3/259;

14

15

Sakthi.K

Sivan.C

Beatuinay
en eral
Krishnagiri.

1/287,
K.V.Kovilkottai,

Sokkadi Village,
Moramadugu Post,
Krishnagiri.

Independent

SaVNe

G.KARPAGAVALLI
RETURNING OFFICER,

RETURNING

OFFICER,

53,Krishnagiri Assembly Constituency/
Revenue Divisional Officer,

KrishnagIri.

Date : 22.03.2021
Place : Krishnagiri.

ou

Television

