FORM 7-A
LIST OF CONTESTING CANDIDATES.
(See rule 10(1) of the Conduct of Elections Rules,1961)
Election to the Tamil Nadu Legislative Assembly
From the 048.Ambur Assembly Constituency
SI.No. | Name of Candidate |

1

2

Candidate's photo | Address of candidate | Party affiliation | Symbol allotted.

3

4

5

6

(i) Candidates of recognised National and State Political parties.
1

NAZAR
MOHAMED.

K

No.26/30,

All India

Sikkandar Tippu

Anna

Town, Ambur
Taluk, Tirupathur

Munnetra

street, Ambur

TWO

LEAVES

Dravida

Denier eapedsee | aacdam

2

WAZEER

AHMED.

J

No.34, Niyamath | Bahujan

street,

Pernambut,
Vellore District

Samaj

Party

ELEPHANT

635810.

3

VILWANATHAN.

A.C

No. 3/15 Raja

Dravida

veethi,

Munnetra

Guttakindoor

village,
Mittalam Post,
Ambur Taluk-

RISING SUN

Kazhagam

635811.

(ii) Candidates of registered Political Parties
(other than recognised National and State Political parties)
4

ASHOK KUMAR.

C

No.73,

Anaithu

Mariyamman Koil | Makkal

Street,

Periyamalayampat

tu village,

Thennampattu
post, Ambur
Taluk, Tirupathur

District-635802

Puratchi

katchi

GLASS

TUMBLER

UMAR FAROOK.
ASA

No.14/32,
Social
Gurusami Street, | Democratic

Sembiyam,

Party Of

Perampur,
Chennai - 600011

KAREEM
BASHA. A

PARTHIBAN. V

India

No.39/2, Eidga
Rashtriya
Road, Ambur
Ulama
Town, Tirupathur | Council
District 635802

SHOE

176, Nagalingam_ | All India
Koil Street,
Youth
Vinnamangalam
Development

CUP & SAUCER

No.10,

HAT

Village, Ambur
Taluk, Thirupathur
District.

MANI. S.P

PRESSURE
| COOKER

Samiyarmadam,

Vivekanandar
First Street,

Party

All

| Pensioner’s
Party

Krishnapuram,
Byepass Road,
Ambur Town,
Tirupathur District
MAHARUNNISH

A

- 635802

No.6, New

Jalalpettai,
2-nd Main Road,

5th Cross Street,
Ambur.
Tirupathur
District-635802

Naam

Tamilar
Katchi

GANNA KISAN

10

:

|RAJA.S

No.253,

Makkal

Asanampattu
Kamarajar

Kambikollai,

Needhi

BATTERY

Nagar,

Ambur -635 802.
(iii) Other Candidates.

11

MURUGAN. L

2nd Tharvazhi,

Independent | TRACTOR

Ambur Town,

CHALATA

Tirupathur

KISAN

District-635802

12

RAJINI KANTH. P

Kumaresan

Street, Kaspa-B,

Independent | AUTO

RICKSHAW

Ambur Town,
Tirupathur District
635802

13

GEORGE. V.A

Nowss3,

Veeraragavapura

Independent |

m, Somalapuram
Village, Ambur
Taluk, Tirupathur

District.

Place:

Ambur

Date:

22.03.2021

a

ISHNA
Returnin

nA
satis

048.Ambur Assembly
eae a
Special Deputy Collector (SSS),
Tirupathur District.

BANGLES

