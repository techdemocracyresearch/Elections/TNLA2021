FORM 7-A
LIST OF CONTESTING CANDIDATES.
(See rule 10(1) of the conduct of Elections Rules, 1961)
Election to the Assembly Constituency from the
226. Palayamkottai Assembly Constituency.
Serial
Number

Name of Candidate

Photo

Address of
candidate

Party affiliation

Symbol
allotted

1

2

3

4

5

6

(i)

Candidates of recognized National and State Political parties

1.
ABDULWAHAB,

M.

2D.

JERALD, K.J.C.

A)

framers | pwns
28A/79, MGP
S

Se
e

thi Street

tee
:
Tirunelveli 627004

ee
S

No.4, Sankar

ona

Tirunelveli District627 002
;

ii) Candidates of registered Political Parties.
(other than recognized National and State Political parties)

201, Hameempuram

3.

2nd Street,
FATHIMA,

Melapalayam

A.

MUNNETRA
KAZHAGAM

A

No
MUNNETRA
KAZHAGAM

KATCHI

Ganna Kisan

16A,Kulavanigarpur | MAKKAL

am Main road,

Palayamkottai,
Tirunelveli-627001

PREMNATH, D.

Ds

NEEDHI

MOHAMED MUBARAK,

Welapalevary

V.MS.

6.

eee

A

ated

Tirunelveli 627005 | parTy OF INDIA
B96, 6th Cross
VEERATH
Street,
NGO ‘A’ | THIYAGI

{= =}
RAJA

MAIAM

Battery Torch

5/121 Sappani

Aalim East Street,

Tot

Colony,

we

'

Palayamkottai,
Tirunelveli627007

Two Leaves

NAAM TAMILAR

Tirunelveli-627005

4.

Rising Sun

VISWANATHAD

OSS
THOZHILALARK
AL KATCHI

Pressure
Cooker

Pestle and
Mortar

12, B.V. NAGAR
13TH STREET,
NANGANALLUR,
CHENNAI —
600061

VEERASUBRAMANIYAN

ANAITHINTHIYA
JANANAYAKA
PATHUKAPPU
KAZHAGAM

Tube Light

iii) Other candidates
8..

110E/1A

Sree

INDEPENDENT

madura Brindavan
Street, Barani
Nagar, Vannarpettai,

SADAGOPAN, K.

:

Palayamkottai :

Diamond

Tirunelveli —
627002

INDEPENDENT

13E/17 Kariya

Nayanar Street,
MGR Colony,
Palayamkottai
Tirunelveli —

LEO INFANT RAJ, M.

error
Richa

627002

10.

| | 33/1 Jebamalikai

JOHN SAMUVEL

JESUPATHAM, D.

2nd Street

v

ye

Ba
ea
Thirunelveli

627005

INDEPENDENT

ae
-

Do\% \norh,

Place : Tirunelveli
Date: 22.03.2021.

3

ane

G.
N
RETURNING OFFICER
226 PALAYAMKOTTAI ASSEMBLY CONSTITUENCY AND
COMMISSIONER OF TIRUNELVELI MUNICIPAL
CORPORATION, TIRUNELVELI.

Jrorr
* Applicable in the case of candidates mentioned under categories (i) and (ii) above.
NB: : Under column (1) above the serial numbers of candidates of all the three categories shall be
given consecutively and not separately for each category.

