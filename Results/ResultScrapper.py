import requests
import json
from bs4 import BeautifulSoup
from lxml import html
import pandas as pd

BASE_URL = 'https://results.eci.gov.in/Result2021/ConstituencywiseSTATEIDASSEMBLYID.htm?ac=ASSEMBLYID'


def GetConstituencyUrl(state_id, assembly_id):
    return BASE_URL.replace('STATEID', str(state_id)).replace('ASSEMBLYID', str(assembly_id))


def main():
    result = {'OSN': '', 'Candidate': '', 'Party': '', 'EVM Votes': '',
              'Postal Votes': '', 'Total Votes': '', 'Vote Share': ''}
    state_ids = ['S22', 'S25', 'S11', 'S03', 'U07']
    assembly_id = {'S22': '234', 'S25': '292',
                   'S11': '140', 'S03': '126', 'U07': '30'}

    for state_id in state_ids:
        results = []
        constituencies = int(assembly_id[state_id]) + 1
        for constituency_id in range(1, constituencies):
            url = GetConstituencyUrl(state_id, constituency_id)
            s = BeautifulSoup(requests.get(url).text, 'lxml')
            rows = s.findAll('tbody')[-3].findAll('tr')

            for i in range(3, len(rows)-1):
                result = {}
                result['State_id'] = state_id
                result['Constituency_id'] = constituency_id
                result['Constituency'] = rows[0].text.strip().split('-')[1]
                result['OSN'] = rows[i].findAll('td')[0].text
                result['Candidate'] = rows[i].findAll('td')[1].text
                result['Party'] = rows[i].findAll('td')[2].text
                result['EVM Votes'] = rows[i].findAll('td')[3].text
                result['Postal Votes'] = rows[i].findAll('td')[4].text
                result['Total Votes'] = rows[i].findAll('td')[5].text
                result['Voteshare'] = rows[i].findAll('td')[6].text
                results.append(result)
        with open('results' + state_id + '.json', 'w', encoding='utf8') as json_file:
            json.dump(results, json_file, ensure_ascii=False)

        pdObj = pd.read_json('results' + state_id + '.json')
        pdObj = pdObj.sort_values(
            by=['State_id', 'Constituency_id', 'Total Votes'], ascending=[True, True, False])
        pdObj['Position'] = pdObj.groupby('Constituency_id')[
            'Total Votes'].rank(ascending=False)
        pdObj['Margin'] = pdObj.groupby('Constituency_id')[
            'Total Votes'].diff()
        pdObj.to_csv('results' + state_id + '.csv')


if __name__ == "__main__":
    main()
