# Code from https://www.geeksforgeeks.org/how-to-extract-images-from-pdf-in-python/
# Data - Free Symbols from https://eci.gov.in/files/file/13021-list-of-free-symbols-with-images-as-on-11th-march-2021/

import fitz
import io
from PIL import Image
  
# STEP 2
# file path you want to extract images from
file = "free symbols_11032021.pdf"
  
# open the file
pdf_file = fitz.open(file)
  
total_images = 0
# STEP 3
# iterate over PDF pages
for page_index in range(len(pdf_file)):
    
    # get the page itself
    page = pdf_file[page_index]
    image_list = page.getImageList()
      
    # printing number of images found in this page
    if image_list:
        print(f"[+] Found a total of {len(image_list)} images in page {page_index}")
        total_images += len(image_list)
    else:
        print("[!] No images found on page", page_index)
    for image_index, img in enumerate(page.getImageList(), start=1):
        
        # get the XREF of the image
        xref = img[0]
        # extract the image bytes
        base_image = pdf_file.extractImage(xref)
        image_bytes = base_image["image"]
        # get the image extension
        image_ext = base_image["ext"]
        name_of_file = str(total_images + image_index - 3) + '.' + base_image["ext"]
        file = open(name_of_file,'wb')
        file.write(image_bytes)
        file.close()



print("Total Images " + str(total_images))
